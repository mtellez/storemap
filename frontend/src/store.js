import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
  strict: true,

  state: {
    tags: [],
  },

  getters: {
    tagifyTags(state) {
      return state.tags.map((tag) => ({
        code: tag.id.toString(),
        value: tag.name,
      }));
    },
  },

  mutations: {
    setTags(state, data) {
      state.tags = data;
    },
  },

  actions: {
    loadTags({ commit }) {
      fetch('/api/label/')
        .then((req) => req.json())
        .then((data) => commit('setTags', data));
    },
  },
});

export default store;
