from django.urls import path
from django.contrib.gis import admin
from django.contrib.auth import logout
from backend.views import (StoreDetail, StoreList, StoreByLabel, LabelList,
                           LabelDetail)
from django.conf.urls import include

from config.api import api

urlpatterns = [
    path('admin/', admin.site.urls, name='admin'),
    path('logout', logout, {'next_page': '/'}, name='logout'),
    path('api/store', StoreList.as_view(), name='store_list'),
    path('api/store/<int:pk>', StoreDetail.as_view(), name='store_detail'),
    path('api/store/by_label', StoreByLabel.as_view(), name='store_bylabel'),
    path('api/label', LabelList.as_view(), name='label_list'),
    path('api/label/<int:pk>', LabelDetail.as_view(), name='label_detail'),
    path('api/', include(api.urls)),
    path('api-auth/', include('rest_framework.urls',
                              namespace='rest_framework')),
]
