import pytest
from django.urls import reverse
from django.db import IntegrityError
from backend import models


def get_response(api_client, url, reverse_mode=True):
    if reverse_mode:
        url = reverse(url)
    response = api_client.get(url)

    return response


def post_response(api_client, url, data, format='json'):
    url = reverse(url)
    response = api_client.post(url, data, format)

    return response


def demo_stores(create_store):
    stores = []
    stores.append(create_store(name='Ferretería Onofre'))
    stores.append(create_store(name='Casa Ahued'))

    return stores


def demo_labels(create_label):
    labels = []
    labels.append(create_label(name='restaurante'))
    labels.append(create_label(name='farmacia'))

    return labels


@pytest.mark.django_db
def test_store(api_client):
    response = get_response(api_client, 'store_list')
    assert response.status_code == 200


@pytest.mark.django_db
def test_store_empty(api_client):
    response = get_response(api_client, 'store_list')
    assert response.data['features'] == []


@pytest.mark.django_db
def test_store_invalid_values(api_client, create_store):
    with pytest.raises(IntegrityError):
        create_store(name=None,
                     address=None,
                     location=None,
                     contact_name=None,
                     phone_number=None)


@pytest.mark.django_db
def test_store_default_values(api_client, create_store):
    point = models.default_point()
    response = post_response(api_client, 'store_list',
                             {'address': 'Av. Xalapa 105'})
    x, y = response.data['geometry']['coordinates']
    assert response.status_code == 201
    assert response.data['properties']['name'] == 'Miscelánea'
    assert x == point.x and y == point.y


@pytest.mark.django_db
def test_store_array(api_client, create_store):
    demo_stores(create_store)
    response = get_response(api_client, 'store_list')
    assert len(response.data) == 2


@pytest.mark.django_db
def test_store_detail(api_client, create_store):
    stores = demo_stores(create_store)
    pk = stores[0].pk
    response = get_response(api_client, f'/api/store/{pk}', reverse_mode=False)
    assert response.data['properties']['name'] == 'Ferretería Onofre'


@pytest.mark.django_db
def test_label(api_client):
    response = get_response(api_client, 'label_list')
    assert response.status_code == 200


@pytest.mark.django_db
def test_label_empty(api_client):
    response = get_response(api_client, 'label_list')
    assert response.data == []


@pytest.mark.django_db
def test_label_new(api_client, create_label):
    data = {'name': 'restaurante', 'description': 'comida casera'}
    response = post_response(api_client, 'label_list', data)
    label = models.Label.objects.get(name='restaurante')
    assert response.status_code == 201
    assert response.data['name'] == label.name


@pytest.mark.django_db
def test_label_invalid_values(api_client, create_label):
    with pytest.raises(IntegrityError):
        create_label(name=None, description='comida mexicana')


@pytest.mark.django_db
def test_label_array(api_client, create_label):
    demo_labels(create_label)
    response = get_response(api_client, 'label_list')
    assert len(response.data) == 2


@pytest.mark.django_db
def test_label_detail(api_client, create_label):
    labels = demo_labels(create_label)
    pk = labels[0].pk
    response = get_response(api_client, f'/api/label/{pk}', reverse_mode=False)
    assert response.status_code == 200
    assert response.data['name'] == 'restaurante'


@pytest.mark.django_db
def test_store_by_label(api_client, create_store, create_label):
    labels = demo_labels(create_label)
    stores = demo_stores(create_store)
    pk = labels[0].pk
    stores[0].label.add(labels[0])
    stores[0].save()
    response = api_client.get('/api/store/by_label', {'data': pk})

    assert response.data['features'][0]['id'] == stores[0].pk


@pytest.mark.django_db
def test_store_by_label_invalid_values(api_client, create_store):
    demo_stores(create_store)
    response = api_client.get('/api/store/by_label', {'data': 0})
    assert response.status_code == 404
    response = api_client.get('/api/store/by_label', {'data': 'lorem ipsum'})
    assert response.status_code == 400
