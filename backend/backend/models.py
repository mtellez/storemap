"""Define backend's models.
"""
from django.contrib.gis.db import models
from django.contrib.gis.geos import Point


class Label(models.Model):
    """This models store the labels assigned to a store.

    This labels can be categories, types of food, etc.
    """
    name = models.CharField(max_length=50, unique=True)
    description = models.CharField(max_length=800,
                                   blank=True,
                                   null=True,
                                   default=None)

    def __str__(self):
        return self.name


def default_point():
    return Point(x=-96.91984176635742, y=19.547658036361266, srid=4326)


class Store(models.Model):
    """Keep track of stores info, like location and contact name.
    """
    name = models.CharField(max_length=128, default='Miscelánea')
    address = models.CharField(max_length=128,
                               blank=True,
                               null=True,
                               default=None)
    location = models.PointField(default=default_point)
    contact_name = models.CharField(max_length=64,
                                    blank=True,
                                    null=True,
                                    default=None)
    phone_number = models.CharField(max_length=12,
                                    blank=True,
                                    null=True,
                                    default=None)
    email = models.EmailField(blank=True, null=True, default=None)
    url = models.URLField(blank=True, null=True, default=None)
    opening_hour = models.TimeField(blank=True, null=True, default=None)
    closing_hour = models.TimeField(blank=True, null=True, default=None)
    has_delivery = models.BooleanField(default=False)
    active = models.BooleanField(default=True)
    label = models.ManyToManyField(Label)

    def __str__(self):
        return f'{self.name} - {self.location}'
